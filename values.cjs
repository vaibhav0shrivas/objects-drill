
var _ = require('underscore');

// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions



function values(obj) {
    let result=[];
    
    let allValues = _.values(obj); // Return all of the values of the object's own properties.
    for(let index=0; index<allValues.length; index++){// Ignore functions
       if(typeof allValues[index] === 'function'){
        continue;
       }
       result.push(allValues[index]);
    }
    return result; // Ignore functions
    
    // Based on http://underscorejs.org/#values
}

module.exports =  values;