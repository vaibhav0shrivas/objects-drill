var _ = require('underscore');


function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    let result ={};
    let keys = _.keys(obj);

    for( let index = 0; index < keys.length; index++){
        if(typeof obj[keys[index]] === 'function' || typeof obj[keys[index]] === 'object'){
            continue;
        }
        result[obj[keys[index]] ]= keys[index];

    }
    return result;
}

module.exports = invert;