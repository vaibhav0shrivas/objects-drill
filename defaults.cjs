var _ = require('underscore');


function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    let keysOfDefaultProps =_.keys(defaultProps);

    for( let index = 0; index < keysOfDefaultProps.length; index++){
        if(typeof obj[keysOfDefaultProps[index]] === 'undefined'){
            
            obj[keysOfDefaultProps[index]]= defaultProps[keysOfDefaultProps[index]];

            
            
        }
        

    }
    return obj;
}

module.exports = defaults;