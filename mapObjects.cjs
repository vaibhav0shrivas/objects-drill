
var _ = require('underscore');

function mapObject(obj, cb) {
    let result ={...obj};// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    let keys = _.keys(obj);

    for( let index = 0; index < keys.length; index++){
        if(typeof obj[keys[index]] === 'function' || typeof obj[keys[index]] === 'object'){
            continue;
        }
        result[keys[index]]= cb(obj[keys[index]] );

    }
    return result;
    
    // http://underscorejs.org/#mapObject
}


module.exports = mapObject;