var _ = require('underscore');

function pairs(obj) {
    let result = [];// Convert an object into a list of [key, value] pairs.
    let keys = _.keys(obj);

    for( let index = 0; index < keys.length; index++){
        
        result.push([keys[index],obj[keys[index]]]);

    }
    return result;

    // http://underscorejs.org/#pairs
}

module.exports = pairs;