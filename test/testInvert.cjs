const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const testInvert = require('../invert.cjs');


const result = 
// testInvert(testObject);
// testInvert({});
// testInvert(null);
// testInvert();
// testInvert({a:1,b:4,c:{x:1,y:33}}); 
testInvert({a:1,b:23,
    c(){
        console.log("this is function c");
    },d:"hello",x:["hi"]
    });


console.log(result);