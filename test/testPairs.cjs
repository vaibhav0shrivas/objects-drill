const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const testPairs = require('../pairs.cjs');

const result = 
// testPairs(testObject);
// testPairs({});
// testPairs({a:1,b:4,c:{x:1,y:33}});
testPairs({a:1,b:23,
    c(){
        console.log("this is function c");
    }
    });
// testPairs(null);
// testPairs();
console.log(result);
