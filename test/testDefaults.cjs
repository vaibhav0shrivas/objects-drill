const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const testDefaults = require('../defaults.cjs');


const result = 

testDefaults(testObject,{flavor: "vanilla", sprinkles: "lots"}); 
// testDefaults({flavor: "chocolate"},{flavor: "vanilla", sprinkles: "lots"});


console.log(result);