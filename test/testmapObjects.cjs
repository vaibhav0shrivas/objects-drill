const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const testMapObject = require('../mapObjects.cjs');

function fun(val){ return val + 2};
const result = 
// testMapObject(testObject,fun);
// testMapObject({});
// testMapObject(null);
// testMapObject();
testMapObject({a:1,b:4,c:{x:1,y:33}},fun); 
// testMapObject({a:1,b:23,
//     c(){
//         console.log("this is function c");
//     },d:"hello",x:["hi"]
//     },fun);


console.log(result);