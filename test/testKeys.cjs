const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const testKeys = require('../keys.cjs');

const result = 
// testKeys(testObject);
// testKeys({});
// testKeys({a:1,b:4,c:{x:1,y:33}});
// testKeys({a:1,b:23,
//     c(){
//         console.log("this is function c");
//     }
//     });
// testKeys(null);
testKeys();
console.log(result);

