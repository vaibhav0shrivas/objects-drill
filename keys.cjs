
var _ = require('underscore');

// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions



function keys(obj) {
    let result=[];
    
    result = _.keys(obj); // Retrieve all the names of the object's properties.
    
    return result; // Return the keys as strings in an array.
    
    // Based on http://underscorejs.org/#keys
}

module.exports =  keys;